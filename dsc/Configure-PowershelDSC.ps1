Write-Host "Enabling file downloads in internet options and Tls12 protocol"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$internetZonePath = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
$fileDownloadKey = "1803"
New-ItemProperty -Path $internetZonePath -Name $fileDownloadKey -Value 0 -PropertyType DWORD -Force

Write-Host "Install nuget package provider"
Install-PackageProvider -Name NuGet -RequiredVersion 2.8.5.201 -Force

Write-Host "Register default PS Repo"
Register-PSRepository -Default

Write-Host "Install required modules"
Install-Module xComputerManagement
Install-Module xSystemSecurity
Install-Module xRemoteDesktopAdmin
Install-Module xPSDesiredStateConfiguration