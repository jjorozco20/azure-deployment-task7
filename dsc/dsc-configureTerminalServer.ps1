Configuration BaseComputerConfiguration
{
    param
    (
		[Parameter(Mandatory)]
        [string]$MachineName,

        [Parameter(Mandatory)]
        [pscredential]$Credential

    )
    
	Import-DscResource -Module xComputerManagement
	Import-DSCResource -Module xSystemSecurity
	Import-DscResource -Module xRemoteDesktopAdmin
    Import-DscResource -Module xPSDesiredStateConfiguration
    
	Node localhost
    {        
        
        xUAC DisableUAC
        {
            Setting = "NeverNotifyAndDisableAll"
        }

		xRemoteDesktopAdmin EnableRemoteDesktop
        {
           Ensure = 'Present'
           UserAuthentication = 'Secure'
        }
		
        User RemoteUser
        {
            Ensure = "Present"  # To ensure the user account does not exist, set Ensure to "Absent"
            UserName = "RemoteUser"
            Password = $Credential # This needs to be a credential object
        }

		xGroup RDPGroup
        {
            Ensure = 'Present'
            GroupName = "Remote Desktop Users"
            Members = @("RemoteUser")
            Credential = $Credential
            DependsOn = "[User]RemoteUser" # Add remote user first
        }

		#Ensures .Net-Framework 4.5 is present
		xWindowsFeature Net-Framework-45-Core
        {
            Ensure = "Present"
            Name = "Net-Framework-45-Core"
        }
        
        # Ensure Spooler Service is running
        Service ServiceSpooler
        {
            Name        = "Spooler"
            StartupType = "Automatic"
            State       = "Running"
        } 

        xComputer NewNameAndWorkgroup
        {
            Name          = $MachineName
            WorkGroupName = 'TrainingGroup'
        }
	}
}

$EncryptedPass = "76492d1116743f0423413b16050a5345MgB8AHEALwByAEIARAB0AGkAbwBMAGYAZQB2AG8AQgA3AEYAMgBqAGgANAB4AFEAPQA9AHwAOQA2AGUAZQA1ADQAYQAyAGIANwAyAGMAYgAzADIAMwA3AGQANwBiAGQAMwBiAGEAYQBmADIAMQBiADQAMgAwAGYAYwAzADMAMwA0AGQANQAwADAANQAyADMAYwBiAGUAYQAzADQAMQBhAGQAMAAyADEAZgAxADgAMgAwADgAMQAzAGQANQA2ADIAMgAzADEAMwA0AGMANwBmAGYAMgBkADMAZQBiADAAYQBhADkAYgBkAGYANwAxAGIAOQAwADMANQAxADMAZQBlAGIAMABmAGUAOABkAGUAMwA0ADAAOABkAGQAYgAwADcANQA4ADAANQA1AGEANAAxADAANQBmAA=="
$SecureStringPass = ConvertTo-SecureString -String $EncryptedPass -Key (1..16)

$User = "DevopsAdmin"
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $User, $SecureStringPass

$ConfigData = @{
    AllNodes = @(
        @{
            NodeName = "localhost"
            # Allows credential to be saved in plain-text in the the *.mof instance document.
            PSDscAllowPlainTextPassword = $true
        }
    )
}

BaseComputerConfiguration -MachineName "WS2016-Train" -Credential $Credential -ConfigurationData $ConfigData

Start-DscConfiguration .\BaseComputerConfiguration -wait