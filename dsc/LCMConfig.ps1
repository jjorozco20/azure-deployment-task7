<#
.SYNOPSIS
Configure LCM.

.DESCRIPTION
Configure LCM.
The parameters have the original default values.
Only the value for RebootIfNeeded was changed to true.
Read more about LCM: https://docs.microsoft.com/en-us/powershell/dsc/metaconfig

.PARAMETER NodeName
Hostname of the machine where the configuration will be applied.

.PARAMETER DebugMode
Possible values are None, ForceModuleImport, and All. 

.PARAMETER RebootIfNeeded
Set this to $true to automatically reboot the node after a configuration that requires reboot is applied.

.PARAMETER ConfigurationMode
Specifies how the LCM actually applies the configuration to the target nodes. 
Possible values are "ApplyOnly","ApplyandMonitior", and "ApplyandAutoCorrect".

.PARAMETER AllowModuleOverwrite
$TRUE if new configurations downloaded from the configuration server are allowed to overwrite the old ones on the target node.

.PARAMETER RefreshMode
Specifies how the LCM gets configurations. The possible values are "Disabled", "Push", and "Pull". 

.PARAMETER ActionAfterReboot
Specifies what happens after a reboot during the application of a configuration. 
The possible values are "ContinueConfiguration" and "StopConfiguration". 

.EXAMPLE
. .\LCMConfig.ps1
LCMConfig
Set-DscLocalConfigurationManager -Path .\LCMConfig

#>
Configuration LCMConfig
{
    param
    (        
        [string]$DebugMode = 'None',

        [bool]$RebootIfNeeded = $true,

        [string]$ConfigurationMode = 'ApplyOnly',

        [bool]$AllowModuleOverwrite = $true,

        [string]$RefreshMode = 'Push',

        [string]$ActionAfterReboot = 'ContinueConfiguration'
    )

    Node localhost
    {
        LocalConfigurationManager
        {
            DebugMode = $DebugMode
            RebootNodeIfNeeded = $RebootIfNeeded
            ConfigurationMode = $ConfigurationMode
            AllowModuleOverwrite = $AllowModuleOverwrite
            RefreshMode = $RefreshMode
            ActionAfterReboot = $ActionAfterReboot
        }
    }
}