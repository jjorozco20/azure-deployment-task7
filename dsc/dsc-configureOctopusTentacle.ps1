Configuration OctopusTentacle{

    Import-DscResource -Module OctopusDSC

    Node localhost
    {
        cTentacleAgent OctopusTentacle
        {
            Ensure = "Present"
            State = "Started"

            Name = "Tentacle"
            OctopusServerUrl = "http://octopus.local"
            ApiKey = "API-XXXXXXXXXXXXXXXXXX"
            RegisterWithServer = $True
            Environments = "Instances-Inventory"
            Roles = "Managed"
            ListenPort = 10943
            CommunicationMode = "Poll"
        }
    }
}

OctopusTentacle

Start-DscConfiguration .\OctopusTentacle -wait