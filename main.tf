resource "azurerm_resource_group" "task56_rg"{
    name = "Task56RG"
    location = var.location
}

resource "azurerm_availability_set" "task5_set"{
    name = "Task5Set"
    location = var.location
    resource_group_name = azurerm_resource_group.task56_rg.name
}

resource "azurerm_virtual_network" "task5_vnet"{
    name = "Task5VNet"
    address_space = ["10.0.0.0/16"]
    location = var.location
    resource_group_name = azurerm_resource_group.task56_rg.name
}

resource "azurerm_subnet" "task5_subnet"{
    name = "Task5Sb"
    resource_group_name = azurerm_resource_group.task56_rg.name
    virtual_network_name = azurerm_virtual_network.task5_vnet.name
    address_prefixes = ["10.0.0.0/16"]

}

resource "azurerm_public_ip" "publicip" {
  name                = "PublicIP"
  resource_group_name = azurerm_resource_group.task56_rg.name
  location            = azurerm_resource_group.task56_rg.location
  allocation_method   = "Static"
  sku = "Basic"

  tags = {
    environment = var.env
  }
}


resource "azurerm_network_security_group" "general_security" {
  name = "${var.env}-windows-vm-nsg"
  location            = azurerm_resource_group.task56_rg.location
  resource_group_name = azurerm_resource_group.task56_rg.name
  security_rule {
    name                       = "allow-rdp"
    description                = "allow-rdp"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = var.sourceip
    destination_address_prefix = "*" 
  }
}

resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.windows_nic.id
  network_security_group_id = azurerm_network_security_group.general_security.id
}

resource "azurerm_network_interface" "windows_nic" {
  name                = "WindowsNIC"
  location            = var.location
  resource_group_name = azurerm_resource_group.task56_rg.name

  ip_configuration {
    name                          = "general"
    subnet_id                     = azurerm_subnet.task5_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.publicip.id
  }
}

data "template_file" "init" {
  template = "${file("change-name.ps1")}"
}


resource "azurerm_windows_virtual_machine" "windows_server" {
  name                = "Task5-VM"
  resource_group_name = azurerm_resource_group.task56_rg.name
  location            = var.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = var.password # move to vault or to get it from variables
  depends_on          = [azurerm_network_interface.windows_nic]
  network_interface_ids = [
    azurerm_network_interface.windows_nic.id,
  ]
  custom_data = base64encode(data.template_file.init.rendered)
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  # provisioner "file" {
  #   source      = "C:\\Users\\NHAFLM\\Desktop\\Repos\\Infrastructure\\change-name.ps1"
  #   destination = "C:\\WindowsAzure\\change-name.ps1"
  # }

  # provisioner "remote-exec" {
  #   inline = [
  #     "powershell.exe -ExecutionPolicy Unrestricted -File C:\\WindowsAzure\\change-name.ps1"
  #   ]
  # }

  #  connection {
  #   type     = "winrm"
  #   user     = "adminuser"
  #   password = var.password
  #   host     = resource.azurerm_public_ip.publicip.ip_address
  # }
  

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}

resource "azurerm_virtual_machine_extension" "execute-userdata" {
  #depends_on=[azurerm_windows_virtual_machine.web-windows-vm]
  name = "ChangeVMName"
  virtual_machine_id = azurerm_windows_virtual_machine.windows_server.id
  publisher = "Microsoft.Compute"
  type = "CustomScriptExtension"
  type_handler_version = "1.9"
  settings = <<SETTINGS
   { 
     "commandToExecute": "powershell.exe rename-item  C:\\AzureData\\CustomData.bin -newname CustomData.ps1; powershell.exe [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; powershell.exe Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force -Scope CurrentUser; powershell.exe Install-Module -Name xComputerManagement -MinimumVersion 4.1.0.0 -Force; powershell.exe Start-Process -path C:\\AzureData\\CustomData.ps1;"
   } 
  SETTINGS
}