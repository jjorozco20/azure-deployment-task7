#######################################################
### Configure System environment variables for azure ### 
########################################################
$subscriptionId = "d6396486-ba25-4942-80bb-0ba9dda08c4d" #Visual studio profesional

## Import AzureRM Module
$module = Get-Module -ListAvailable -Name az
if(-not $module){
    Install-Module -Name az -AllowClobber -Force
}

Add-Type -AssemblyName System.Web
$ServicePrincipalName = "AzureDevOps"

# Login to Azure
Connect-AzAccount -Subscription $subscriptionId

# Get azure context
#$tenantId = (Get-AzContext).Tenant.Id

# Verify if service principal already exists
$servicePrincipal = Get-AzADServicePrincipal -ServicePrincipalName "http://$ServicePrincipalName"
# Remove service principal
#Remove-AzADServicePrincipal -ServicePrincipalName "http://$ServicePrincipalName"

if($servicePrincipal){
    Write-Host "Service principal 'http://$ServicePrincipalName' already exists"
}else{
    Write-Host "Adding Service Principal..."
    #By default created with role 'Contributor'
    $servicePrincipal = New-AzADServicePrincipal -DisplayName $ServicePrincipalName
    Start-Sleep 20
    $servicePrincipal
    #New-AzRoleAssignment -RoleDefinitionName Contributor -ServicePrincipalName $servicePrincipal.ApplicationId
}
# Get subscription values
$subscription = Get-AzSubscription -SubscriptionId $subscriptionId
#$secureSecret = ConvertFrom-SecureString -SecureString $servicePrincipal.secret
$decryptedSecret = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($servicePrincipal.secret))

# Set environment variables
[System.Environment]::SetEnvironmentVariable('ARM_SUBSCRIPTION_ID', $subscription.Id, [System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('ARM_CLIENT_ID', $servicePrincipal.ApplicationId, [System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('ARM_CLIENT_SECRET', $decryptedSecret, [System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('ARM_TENANT_ID', $subscription.TenantId, [System.EnvironmentVariableTarget]::Machine)

Write-Host "Azure Environment Variables were configured"
Write-Host "#######################################################################################"
Write-Host "# AZURE_TENANT_ID: " $subscription.TenantId
Write-Host "#######################################################################################"
Write-Host "# AZURE_CLIENT_ID: " $servicePrincipal.ApplicationId
Write-Host "#######################################################################################"
Write-Host "# AZURE_CLIENT_SECRET: " $clientSecret
Write-Host "#######################################################################################"
Write-Host "# AZURE_SUBSCRIPTION_ID: " $subscription.Id
Write-Host "#######################################################################################"