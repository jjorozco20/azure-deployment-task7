
Configuration NewNameVM {

Import-DscResource -ModuleName xComputerManagement

Node localhost
{
    xComputer NewMachineName #ResourceName
    {
        Name = $VMName
    }
}
}

Configuration LCMConfig
{
    param
    (        
        [string]$DebugMode = 'None',

        [bool]$RebootIfNeeded = $true,

        [string]$ConfigurationMode = 'ApplyOnly',

        [bool]$AllowModuleOverwrite = $true,

        [string]$RefreshMode = 'Push',

        [string]$ActionAfterReboot = 'ContinueConfiguration'
    )

    Node localhost
    {
        LocalConfigurationManager
        {
            DebugMode = $DebugMode
            RebootNodeIfNeeded = $RebootIfNeeded
            ConfigurationMode = $ConfigurationMode
            AllowModuleOverwrite = $AllowModuleOverwrite
            RefreshMode = $RefreshMode
            ActionAfterReboot = $ActionAfterReboot
        }
    }
}

# Compile the configuration file to a MOF format
LCMConfig
Set-DscLocalConfigurationManager -Path .\LCMConfig -Force

# Change name
$VMName = 'Task5NewName'
NewNameVM
#Run the configuration on localhost
Start-DscConfiguration -Path .\NewNameVM -Wait -Force -Verbose