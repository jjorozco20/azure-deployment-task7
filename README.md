# infrastructure-code

Azurern Infrastructure as code initial(students can use it as a template for their own projects infra).
It deploys a small infra using terraform, set some security settings, installs the gitlab runner and syncs it with a given project gitlab CICD

##Steps to deploy:
1. Dowload terraform 1.0.0
2. Clone the repo
3. Open provider file and setup your service principal keys(How to get sevice principal keys: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret)
4. On the root folder initialize the terraform project: 
```
terraform init
```
5. Verify your environment is ready by executing a plan:
```
terraform plan -var-file="terraform.tfvars"
```